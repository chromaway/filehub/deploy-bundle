#!/bin/bash

echo "Removing folders..."
rm -rf ./filehub
rm -rf ./filechain
rm -rf ./gateway-api
rm -rf ./managed-mode-single-provider/directory-chain
rm -rf ./managed-mode-single-provider/management-console

echo "Removing containers..."
docker rm filehub_directory-chain -f -v &> /dev/null
docker rm filehub_postgres -f -v &> /dev/null
docker rm filehub_gateway-api -f -v &> /dev/null