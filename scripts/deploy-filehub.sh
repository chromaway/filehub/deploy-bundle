#!/bin/bash

echo "Setting up Filehub..."
cd ./filehub

DIRECTORY_CHAIN_BRID=$1
IS_LINUX=$2
USE_RATE_LIMITER=$3
DOMAIN=$4
ENABLE_PAYMENTS=$5
ADMIN_PUBKEY=$6

# Install
echo "Filehub | CHR Install..."
chr install

# Build
echo "Filehub | CHR Build..."
chr build

# Deploy
echo "Filehub | CHR Deployment..."
DEPLOYMENT_BRID_LINE=$(grep -n 'brid' chromia.yml | cut -d ':' -f1 | head -n1)
DOMAIN_LINE=$(grep -n 'url' chromia.yml | cut -d ':' -f1 | head -n1)
RATE_LIMITER_LINE=$(grep -n 'active' chromia.yml | sed -n '3p' | cut -d ':' -f1)
PAYMENTS_LINE=$(grep -n 'payments_enabled' chromia.yml | sed -n '3p' | cut -d ':' -f1)
ADMIN_PUBKEY_LINE=$(grep -n 'admin_pubkey' chromia.yml | sed -n '3p' | cut -d ':' -f1)

if [[ $IS_LINUX = true ]]
then
  sed -i "${DOMAIN_LINE}s/.*/    url: http:\/\/${DOMAIN}:7740/" chromia.yml
  sed -i "${DEPLOYMENT_BRID_LINE}s/.*/    brid: x\"${DIRECTORY_CHAIN_BRID}\"/" chromia.yml
  sed -i "${RATE_LIMITER_LINE}s/.*/          active: ${USE_RATE_LIMITER}/" chromia.yml
  sed -i "${PAYMENTS_LINE}s/.*/          payments_enabled: ${ENABLE_PAYMENTS}/" chromia.yml
  if [[ ! -z "$ADMIN_PUBKEY" ]]; then
    sed -i "${ADMIN_PUBKEY_LINE}s/.*/        admin_pubkey: ${ADMIN_PUBKEY}/" chromia.yml
  fi
else
  sed -i '' "${DOMAIN_LINE}s/.*/    url: http:\/\/${DOMAIN}:7740/" chromia.yml
  sed -i '' "${DEPLOYMENT_BRID_LINE}s/.*/    brid: x\"${DIRECTORY_CHAIN_BRID}\"/" chromia.yml
  sed -i '' "${RATE_LIMITER_LINE}s/.*/          active: ${USE_RATE_LIMITER}/" chromia.yml
  sed -i '' "${PAYMENTS_LINE}s/.*/        payments_enabled: ${ENABLE_PAYMENTS}/" chromia.yml
  if [[ ! -z "$ADMIN_PUBKEY" ]]; then
    sed -i '' "${ADMIN_PUBKEY_LINE}s/.*/        admin_pubkey: ${ADMIN_PUBKEY}/" chromia.yml
  fi
fi

chr deployment create --network local --blockchain filehub_dev --secret ../managed-mode-single-provider/provider/alpha/.chromia/config -y

FILEHUB_COUNTER=0
echo "Check if Filehub is up and running..."
while [ ! "$(docker logs filehub_directory-chain 2>&1 | grep "chain-id=100]: startBlockchain() - Blockchain has been started:")" ]
do
    echo "Waiting for Filehub to start..."
    sleep 2 # Wait for container to start
    let FILEHUB_COUNTER++

    if [ $FILEHUB_COUNTER -ge 30 ]
    then
        break
    fi
done

echo "Filehub deployment completed!"
cd ..