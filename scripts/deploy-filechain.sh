#!/bin/bash

echo "Setting up Filechain..."
cd filechain

DIRECTORY_CHAIN_BRID=$1
FILEHUB_BRID=$2
DOMAIN=$3

# Configure trusted filehub
echo "Updating trusted_filehub_brid..."
yq e ".blockchains.filechain_dev.moduleArgs.fs.trusted_filehub_brid = \"x\\\"${FILEHUB_BRID}\\\"\"" -i chromia.yml

# Install
echo "Filechain | CHR Install..."
chr install

# Build
echo "Filechain | CHR Build..."
chr build

# Update deployment URL and BRID
echo "Updating deployment URL and BRID..."
yq e ".deployments.local.url = \"http://${DOMAIN}:7740\"" -i chromia.yml
yq e ".deployments.local.brid = \"x\\\"${DIRECTORY_CHAIN_BRID}\\\"\"" -i chromia.yml

chr deployment create --network local --blockchain filechain_dev --secret ../managed-mode-single-provider/provider/alpha/.chromia/config -y

FILECHAIN_COUNTER=0
echo "Check if Filechain is up and running..."
while [ ! "$(docker logs filehub_directory-chain 2>&1 | grep "chain-id=101]: startBlockchain() - Blockchain has been started:")" ]
do
    echo "Waiting for Filechain to start..."
    sleep 2 # Wait for container to start
    let FILECHAIN_COUNTER++

    if [ $FILECHAIN_COUNTER -ge 30 ]
    then
        break
    fi
done

echo "Filechain deployment completed!"
cd ..