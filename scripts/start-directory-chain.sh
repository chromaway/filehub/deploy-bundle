#!/bin/bash
cd ./managed-mode-single-provider

echo "Starting Directory Chain..."
docker run -d --name filehub_directory-chain \
              --mount type=bind,source="$(pwd)/config",target=/config,readonly \
              --mount type=bind,source="$(pwd)/directory-chain/build",target=/build,readonly \
              -e POSTCHAIN_DEBUG=true \
              -p 9870:9870/tcp \
              -p 7740:7740/tcp \
              -p 7750:7750/tcp  \
              --add-host=host.docker.internal:host-gateway \
              registry.gitlab.com/chromaway/postchain-chromia/chromaway/chromia-server:3.24.0 run-node  \
               --node-config /config/config.0.properties \
               --chain-id 0  \
               --blockchain-config /build/manager.xml

DIRECTORY_CHAIN_COUNTER=0
while [ ! "$(docker logs filehub_directory-chain 2>&1 | grep "chain-id=0]: startBlockchain() - Blockchain has been started:")" ]
do
    echo "Waiting for Directory Chain to start..."
    sleep 2 # Wait for container to start
    let DIRECTORY_CHAIN_COUNTER++

    if [ $DIRECTORY_CHAIN_COUNTER -ge 30 ]
    then
        break
    fi
done
cd ..