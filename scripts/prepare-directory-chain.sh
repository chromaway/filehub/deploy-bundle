#!/bin/bash
cd ./managed-mode-single-provider

echo "Extracting & preparing Directory Chain..."
NODE_DOMAIN=$1
set -eu

# Locate and unzip Directory Chain and Management Console
C0_SOURCES=$(find . -maxdepth 1 -regex '.*directory-chain-[0-9]*.[0-9]*.[0-9]*.*-sources.tar.gz')
PMC=$(find . -maxdepth 1 -regex '.*management-console-[0-9]*.[0-9]*.[0-9]*.*-dist.tar.gz')

tar xf "$C0_SOURCES"
tar xf "$PMC"

# Build and install Directory Chain
export GENESIS_HOST_NAME=$NODE_DOMAIN
export GENESIS_API_URL=http://${NODE_DOMAIN}:7740
cd directory-chain
chr install
chr build
cd ..