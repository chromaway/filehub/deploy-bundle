#!/bin/bash
cd ./managed-mode-single-provider

echo "Running setup PMC script..."

set -eu

BRID=$1
DOMAIN=$2

cp provider/alpha/.chromia/config.example provider/alpha/.chromia/config
bash management-console/bin/pmc config --file provider/alpha/.chromia/config --set brid="$BRID" --set api.url="http://${DOMAIN}:7740"

export CHROMIA_CONFIG="$(pwd)/provider/alpha/.chromia/config"
PUB_KEY=$(bash management-console/bin/pmc config --get pubkey)

echo "Initializing network..."
bash management-console/bin/pmc network initialize --system-anchoring-config directory-chain/build/system_anchoring.xml \
                                                   --cluster-anchoring-config directory-chain/build/cluster_anchoring.xml

echo "Adding container..."
bash management-console/bin/pmc container add --cluster system \
                                              --name filehub \
                                              --pubkeys $PUB_KEY

cd ..
