#!/bin/bash

echo "Starting Gateway API..."
cd gateway-api
FILEHUB_BRID=$1

cat > src/persistence/data/filehubs.json <<- EOM
{
  "${FILEHUB_BRID}": {
    "brid": "${FILEHUB_BRID}",
    "url": "http://localhost:7740"
  },
  "local": {
    "brid": "${FILEHUB_BRID}",
    "url": "http://localhost:7740",
    "alias": "local"
  }
}
EOM

docker build -t gateway-api .  
docker run -d --name filehub_gateway-api \
              -p 3000:3000 \
              gateway-api

cd ..