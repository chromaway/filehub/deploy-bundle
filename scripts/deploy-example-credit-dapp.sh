#!/bin/bash

echo "Setting up Credit Example Dapp..."
cd credit_example_dapp

DIRECTORY_CHAIN_BRID=$1

# Install
echo "Credit Example Dapp | CHR Install..."
chr install

# Build
echo "Credit Example Dapp | CHR Build..."
chr build

# Deploy
echo "Credit Example Dapp | CHR Deployment..."
DEPLOYMENT_BRID_LINE=$(grep -n 'brid' chromia.yml | cut -d ':' -f1 | head -n1)

if [[ $IS_LINUX = true ]]
then
  sed -i "${DEPLOYMENT_BRID_LINE}s/.*/    brid: x\"${DIRECTORY_CHAIN_BRID}\"/" chromia.yml
else
  sed -i '' "${DEPLOYMENT_BRID_LINE}s/.*/    brid: x\"${DIRECTORY_CHAIN_BRID}\"/" chromia.yml
fi

chr deployment create --network local --blockchain credit_example_dapp --secret ../managed-mode-single-provider/provider/alpha/.chromia/config -y

EXAMPLE_DAPP_COUNTER=0
echo "Check if Credit Example Dapp is up and running..."
while [ ! "$(docker logs filehub_directory-chain 2>&1 | grep "chain-id=102]: startBlockchain() - Blockchain has been started:")" ]
do
    echo "Waiting for Credit Example Dapp to start..."
    sleep 2 # Wait for container to start
    let EXAMPLE_DAPP_COUNTER++

    if [ $EXAMPLE_DAPP_COUNTER -ge 30 ]
    then
        break
    fi
done

echo "Credit Example Dapp deployment completed!"
cd ..