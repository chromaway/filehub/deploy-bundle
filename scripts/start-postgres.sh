#!/bin/bash

echo "Starting Postgres..."
docker run -d --name filehub_postgres  \
              -e POSTGRES_INITDB_ARGS="--lc-collate=C.UTF-8 --lc-ctype=C.UTF-8 --encoding=UTF-8" \
              -e POSTGRES_PASSWORD=postchain \
              -e POSTGRES_USER=postchain  \
              -e POSTGRES_DB=postchain  \
              -p 5432:5432 postgres

POSTGRES_COUNTER=0
while [ ! "$(docker logs filehub_postgres 2>&1 | grep "database system is ready to accept connections")" ]
do
    echo "Waiting for Postgres to start..."
    sleep 1 # Wait for container to start
    let POSTGRES_COUNTER++

    if [ $POSTGRES_COUNTER -ge 10 ]
    then
        break
    fi
done