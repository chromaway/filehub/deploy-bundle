# Filehub and Filechain Deploy Bundle

A Filechain has a dependency to a Filehub, and a Filechain therefore always has to be accompanied to by a Filehub.

This project aims to simplify the deployment process of a Filechain by bundling it.

## Architecture

A Filehub & Filechain can be completely standalone, or it can be deployed in a multi-node setup for redundancy.

![Filehub Architecture](./images/deployment-architecture.png)

## Requirements

1. git
2. docker
3. chromia cli

## Run

Use the deployment script to set up the local development environment:

The start script has three input parameters:

1. _-r_ => Enable rate limiter for Filehub (1 = enabled, 0 = disabled)
2. _-e_ => Which endpoint to use for node e.g. docker/localhost (defaults to localhost)
3. _-a_ => Enable the Gateway API (1 = enabled, 0 = disabled)

```sh
./start.sh # Starts deploy bundle with defaults

./start.sh -r 0 # Rate limiter disabled

./start.sh -e docker # Node URLs configured towards http://docker

./start.sh -a 0 # Gateway API disabled

./start.sh -r 0 -a 0 # Rate limiter & Gateway API disabled

./start.sh -r 0 -e docker -a 0 # Rate limiter & Gateway API disabled and node endpoint configures towards docker
```

The script executes sereval steps to configure the environment:

1. Determining which environment for deploy bundle
2. Resetting folders and docker containers (restarts the environment to clean state)
3. Clones the required repositories for Filehub solution
4. Preparing the Directory Chain (unzips packages, installs and build DC)
5. Starting postgres container
6. Starting directory chain container
7. Installing, building and deploying Filehub
8. Installing, building and deploying Filechain
9. Configures and starts Gateway API container

## Post Installation

Managing of the Filehub can be done using the `filehub` npm package.

```js
const { Filehub, FilehubAdministrator } = require("filehub");

const filehub = new Filehub(filehubUrl, filehubBrid);
const adminKeyPair = new KeyPair(adminPrivateKey);
const filehubAdministrator = new FilehubAdministrator(filehub, adminKeyPair);

// Registers a Filechain in the Filehub, making it eligible for file allocations
await filehubAdministrator.registerFilechain(filechainBrid, filechainUrl);

// Disable a Filechain in the Filehub, making it ineligible for file allocations
await filehubAdministrator.disableFilechain(filechainBrid);

// Enables a disabled Filechain in the Filehub, making it eligible for file allocations again
await filehubAdministrator.enableFilechain(filechainBrid, filechainUrl);
```
