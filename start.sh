#!/bin/bash

# Check dependencies
# Check if yq is installed and version is 4 or above
if ! command -v yq &> /dev/null
then
    echo "yq is not installed. Please install it first."
    exit 1
else
    YQ_VERSION=$(yq --version | awk '{print $NF}' | sed 's/^v//')
    YQ_MAJOR_VERSION=$(echo $YQ_VERSION | cut -d. -f1)
    if [ "$YQ_MAJOR_VERSION" -lt 4 ]; then
        echo "yq version 4 or above is required. Your version: $YQ_VERSION"
        exit 1
    fi
fi

# Input parameter to use rate limiter
# -r Enable rate limiter for Filehub (1 = enabled, 0 = disabled)
# -e Which endpoint to use for node e.g. docker/localhost (defaults to localhost)
# -a Enabled the Gateway API (1 = enabled, 0 = disabled)
# -p Enable payments (1 = enabled, 0 = disabled)
while getopts "r:e:a:p:k:" flag
do
    case "${flag}" in
        r) USE_RATE_LIMITER=${OPTARG};;
        e) NODE_DOMAIN=${OPTARG};;
        a) USE_GW_API=${OPTARG};;
        p) ENABLE_PAYMENTS=${OPTARG};;
        k) ADMIN_PUBKEY=${OPTARG};;
    esac
done

if [[ -z "$USE_RATE_LIMITER" ]]
then
    let USE_RATE_LIMITER=0
fi

if [[ -z "$NODE_DOMAIN" ]]
then
    NODE_DOMAIN="localhost"
fi

if [[ -z "$USE_GW_API" ]]
then
    let USE_GW_API=1
fi

if [[ -z "$ENABLE_PAYMENTS" ]]
then
    let ENABLE_PAYMENTS=false
fi

if [[ -z "$ADMIN_PUBKEY" ]]
then
    ADMIN_PUBKEY=""
fi

# Determining environment
if [ "$(uname)" == "Darwin" ]; then
    echo "Bundle environment: Mac"
    IS_LINUX=false        
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    echo "Bundle environment: Linux"
    IS_LINUX=true
else
    echo "Bundle environment: Other"
    IS_LINUX=false
fi

# Reset before start
echo -e "\033[1;33m### RESET ###\033[0m"
bash scripts/reset-bundle.sh
echo ""

# Clone repositories
echo -e "\033[1;33m### CLONING FILEHUB DEPLOY BUNDLE ###\033[0m"
git clone git@gitlab.com:chromaway/filehub/filehub.git
git clone git@gitlab.com:chromaway/filehub/filechain.git

if [[ $USE_GW_API -eq 1 ]]
then
    git clone git@gitlab.com:chromaway/filehub/gateway-api.git
fi
echo ""

# Setup Directory Chain
echo -e "\033[1;33m### INITIAL PREPARE ###\033[0m"
bash scripts/prepare-directory-chain.sh $NODE_DOMAIN
echo ""

# Start Postgres
echo -e "\033[1;33m### POSTGRES ###\033[0m"
bash scripts/start-postgres.sh
echo ""

# Start Directory Chain
echo -e "\033[1;33m### DIRECTORY CHAIN ###\033[0m"
bash scripts/start-directory-chain.sh
echo ""

DIRECTORY_CHAIN_BRID=$(curl http://${NODE_DOMAIN}:7740/brid/iid_0 -s)

# Setup PMC and network
echo -e "\033[1;33m### PMC ###\033[0m"
sh scripts/setup-pmc.sh $DIRECTORY_CHAIN_BRID $NODE_DOMAIN
echo ""

# Filehub Deployment
echo -e "\033[1;33m### FILEHUB ###\033[0m"
bash scripts/deploy-filehub.sh $DIRECTORY_CHAIN_BRID $IS_LINUX $USE_RATE_LIMITER $NODE_DOMAIN $ENABLE_PAYMENTS $ADMIN_PUBKEY
echo ""

FILEHUB_BRID=$(curl http://${NODE_DOMAIN}:7740/brid/iid_100 -s)

# Filechain Deployment
echo -e "\033[1;33m### FILECHAIN ###\033[0m"
bash scripts/deploy-filechain.sh $DIRECTORY_CHAIN_BRID $FILEHUB_BRID $NODE_DOMAIN
echo ""

FILECHAIN_BRID=$(curl http://${NODE_DOMAIN}:7740/brid/iid_101 -s)

# Example Credit Dapp Deployment
echo -e "\033[1;33m### EXAMPLE CREDIT DAPP ###\033[0m"
bash scripts/deploy-example-credit-dapp.sh $DIRECTORY_CHAIN_BRID
echo ""

CRREDIT_DAPP_BRID=$(curl http://${NODE_DOMAIN}:7740/brid/iid_102 -s)

# Gateway API
if [[ $USE_GW_API -eq 1 ]]
then
    echo -e "\033[1;33m### GATEWAY API ###\033[0m"
    bash scripts/start-gateway-api.sh $FILEHUB_BRID
    echo ""
fi

# SUMMARY
echo ""
echo -e "\033[1;32mSUMMARY:\033[0m"
echo "Filehub URL: http://${NODE_DOMAIN}:7740/brid/iid_100"
echo "Filehub BRID: $FILEHUB_BRID"
echo
echo "Filechain URL: http://${NODE_DOMAIN}:7740/brid/iid_101"
echo "Filechain BRID: $FILECHAIN_BRID"
echo
echo "Example Credit Dapp URL: http://${NODE_DOMAIN}:7740/brid/iid_102"
echo "Example Credit Dapp BRID: $CRREDIT_DAPP_BRID"